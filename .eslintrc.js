module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
        node: true
    },
    extends: [
        'plugin:react/recommended',
        'standard'
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 12
    },
    parser: 'babel-eslint',
    plugins: [
        'react'
    ],
    rules: {
        indent: [
            'error',
            4
        ],
        // note you must disable the base rule as it can report incorrect errors
        'no-use-before-define': 'off',
        'react/prop-types': 'off'
    }
}
