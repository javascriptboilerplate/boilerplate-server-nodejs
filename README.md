# boilerplate-server-nodejs

## Description

Minimal boilerplate for [NodeJs](https://nodejs.org/en/) server development using [ESLint](https://eslint.org/).

## Recommanded tools

1. [Visual Studio Code](https://code.visualstudio.com/)
    1. EditorConfig for VS Code
    2. ESLint (Dirk Baeumer)
    3. REST Client (Huachao Mao)

Note: Add the following settings in Visual Studio code to enable ESLint
```
  "eslint.autoFixOnSave": true,
  "javascript.format.enable": false,
  "editor.formatOnSave": true,
```

## Usage

1. For development: `npm run dev`

    1. Start the server for development and automatically restart after code update.

2. For development testing, use the file **test.rest** with the plugin **REST Client (Huachao Mao)**

    1. Each request can be execute to simulate a client request by clicking the label **"Send Request"** over each request separated by **"###"**

2. For deployment: `npm start`

    1. Start the server for production (TODO: NODE_ENV setting)