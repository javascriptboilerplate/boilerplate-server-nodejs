/**
 * If you install the plugin Rest Client, you can use the file test.rest to execute a
 * client request for each of the following endpoint.
 * In the file test.rest you will see the label 'Send Request' over each request,
 * click this label to execute the request.
 */

const express = require('express')
const bodyParser = require('body-parser')

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

const PORT = 8080
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const HTTP_OK = 200

// Sample data hardcoded for example only
const USERS = [
    { name: 'Martin', age: '46', id: 0 },
    { name: 'Patrick', age: '76', id: 1 },
    { name: 'Mélanie', age: '41', id: 2 }
]

app.get('/', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end('<h1>Home page</h1>')
})

app.get('/users', function (request, response) {
    writeJSONResponse(request, response, USERS)
})

app.get('/users/:id', function (request, response) {
    const id = parseInt(request.params.id)
    writeJSONResponse(request, response, USERS.filter((user) => user.id === id))
})

app.post('/users', function (request, response) {
    // Return the complete request as response for test only
    writeJSONResponse(request, response, request.body)
})

app.put('/users', function (request, response) {
    // Return the complete request as response for test only
    writeJSONResponse(request, response, request.body)
})

app.delete('/users/:id', function (request, response) {
    // Return the specified id parameter as response for test only
    writeJSONResponse(request, response, request.params.id)
})

function writeJSONResponse (request, response, result) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(result, null, 2))
}

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
